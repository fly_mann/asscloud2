package rest2

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"
)

// Webhook DB
var Db FirestoreDatabase

//http://localhost:8080/repocheck/v1/webhooks/ POST + body
func WebHooksHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:

		webHookId := ""
		webHookId = r.URL.Path[23:] // Id
		var iteration *firestore.DocumentIterator
		var webhook Webhook
		var listWebhook []Webhook

		if webHookId != "" { //gets doc
			iteration = Db.Client.Collection("webhooks").Where("ID", "==", webHookId).Documents(Db.Ctx)
		} else {
			iteration = Db.Client.Collection("webhooks").Documents(Db.Ctx)
		}

		for {
			doc, err := iteration.Next() // goes through doc
			if err == iterator.Done {    // if doc done
				break
			}
			if err != nil {
				log.Fatalf("Failed to iterate: %v", err)
			}
			if doc != nil {
				err = doc.DataTo(&webhook)
				listWebhook = append(listWebhook, webhook) // add webhook to listWebhook
			}
		}

		w.Header().Add("content-type", "application/json") // w = w.json
		err := json.NewEncoder(w).Encode(listWebhook)
		if err != nil {
			http.Error(w, "failed to encode webhoks", http.StatusInternalServerError)
			fmt.Println("failed to encode")
		}

	case http.MethodDelete:
		var deleteHook Webhook

		webhookIdentifier := r.URL.Path[23:] // id
		deleteHook.Id = webhookIdentifier    // find webhooks to delete

		err := Db.Delete(&deleteHook)
		if err != nil {
			log.Fatal(err)
		}

	case http.MethodPost:
		var postHook Webhook

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&postHook)
		if err != nil {
			fmt.Println("Failed to decode json from user in post request")
		}

		err = Db.Save(&postHook) //saves the webhook to the DB
		if err != nil {
			log.Fatal(err)
		}

	default:
		fmt.Fprintf(w, "Unknown REST method %v", r.Method)
	}
}
