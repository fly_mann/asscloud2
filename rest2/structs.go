package rest2

import (
	"context"
	"time"

	"cloud.google.com/go/firestore"
)

//  Struct repo, ID, path
type Projects struct {
	ID       int    `json:"id"`
	RepoName string `json:"path_with_namespace"`
	Commit   int    `json:"commits"`
}

type RepoOutPut struct {
	Repositories   []Projects `json:"repositories"`
	Authentication bool       `json:"auth"`
}

//  language and uses
type LangName_Count struct {
	Name  string `json:"language"`
	Count int    `json:"count"`
}

//
type Languages struct {
	LanguageList   []string `json:"languages"`
	Authentication bool     `json:"auth"`
}
type Status struct {
	GitLab   int     `json:"gitLab"`
	Version  string  `json:"version"`
	Database int     `json:"database"` /////
	UpTime   float64 `json:"uptime"`
}
type Webhook struct {
	Id    string `json:"id"`
	Event string `json:"event"`
	Url   string `json:"url"`
	Time  int64  `json:"time"`
}
type Invocation struct {
	Event  string    `json:"event"`
	Params []string  `json:"params"`
	Time   time.Time `json:"time"`
}
type Database interface {
	Init() error
	Close()
	Save(*Webhook) error
	Delete(*Webhook) error
	ReadByID(string) (Webhook, error)
}
type FirestoreDatabase struct {
	ProjectID      string
	CollectionName string
	Ctx            context.Context
	Client         *firestore.Client
}
