package rest2

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"
)

var TimeFromStart time.Time

func getStatus(w http.ResponseWriter, url string) int {
	resp, err := http.Get(url)
	if err != nil {
		http.Error(w, "Something went wrong: "+err.Error(), http.StatusInternalServerError)
	}
	return resp.StatusCode
}

func StatusHandler(w http.ResponseWriter, r *http.Request) {

	stat := &Status{}

	Url_API := "https://git.gvk.idi.ntnu.no/api/v4/projects/"
	w.Header().Add("content-type", "application/json")

	parts := strings.Split(r.URL.Path, "/")
	version := parts[2] // v1
	//url_rest_countries := "https://restcountries.eu/rest/v2/"

	// from discord ------------------------------
	stat.Database = 200 //Assumes it to be ok
	//Doc("0") is for status checks
	_, err := Db.Client.Collection("webhooks").Doc("0").Get(Db.Ctx)
	//_, err := Db.Client.Collection("webhooks").Documents(Db.Ctx)
	if err != nil {
		//Can not get to server for unknown reason
		//Gives a Service Unavailable error
		stat.Database = 503
	}
	//--------------------------------------------

	stat.GitLab = getStatus(w, Url_API)
	//stat.Database = getStatus(w, url_rest_countries)
	stat.Version = version
	stat.UpTime = time.Since(TimeFromStart).Seconds()
	json.NewEncoder(w).Encode(stat)
}
