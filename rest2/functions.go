package rest2

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"google.golang.org/api/iterator"
)

//?limit=int
func getLimit(w http.ResponseWriter, r *http.Request) int {
	var limit int
	u := r.FormValue("limit")
	if u == "" { //no limit, than limit = 5
		limit = 5

	} else {
		v, err := strconv.Atoi(u) //string to int + error
		if err == nil && v >= 1 {
			limit = v
		} else if err == nil && v <= 0 { // "s" == 0?
			http.Error(w, "Something went wrong: limit must be > than 0",
				http.StatusBadRequest)
		} else {
			http.Error(w, "Something went wrong: limit: "+r.FormValue("limit")+
				err.Error(), http.StatusBadRequest)
		}
	}
	return limit
}

//add token, if user have token in url
func getAuth(w http.ResponseWriter, r *http.Request) string {
	var auth string
	u := r.FormValue("auth")
	if u == "" {
		auth = ""

	} else { //&
		auth = "private_token=" + u //?private_token=<your_access_token>
	}
	return auth
}

// gets all the prosjekts
func getProjects(w http.ResponseWriter, r *http.Request, APIURL string) []Projects {
	w.Header().Add("content-type", "application/json") // w = w.json
	PER_PAGE := "?per_page=50"

	if getAuth(w, r) != "" {
		PER_PAGE = PER_PAGE + "&" + getAuth(w, r)
	}

	resp, err1 := http.Get(APIURL + PER_PAGE)
	if err1 != nil {
		http.Error(w, "Something went wrong: "+err1.Error(), http.StatusBadRequest)
	}
	totalInt, _ := strconv.Atoi(resp.Header.Get("X-Total-Pages")) // number of pages int and error
	defer resp.Body.Close()                                       // close resp after handler done

	var projects []Projects // Creating empty array

	for x := 1; x <= totalInt; x++ { //  Goes to page and gets prosjekts
		var p []Projects

		eachPage, err := http.Get(APIURL + PER_PAGE + "&page=" + strconv.Itoa(x))
		if err != nil {
			http.Error(w, "Something went wrong: "+err.Error(), http.StatusBadRequest)
		}

		err = json.NewDecoder(eachPage.Body).Decode(&p) //eachPage.Body -> p
		if err != nil {
			http.Error(w, "Something went wrong: "+err.Error(), http.StatusBadRequest)
		}
		for y := range p {
			projects = append(projects, p[y]) // add all prosjekt inn projekts

		}
	}
	return projects
}

func webHooksInvoke(event string, params []string, currentTime time.Time) {
	iter := Db.Client.Collection("webhooks").Documents(Db.Ctx)
	for { // loops through all webhooks in the database
		var webHook Webhook
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Printf("Failed to iterate: %v", err)
		}
		err = doc.DataTo(&webHook)
		if err != nil {
			log.Printf("Failed to parse database response: %v", err)
		}
		if webHook.Event == event {
			requestWebhook := Invocation{Event: event, Params: params, Time: time.Now()}
			requestBody, err := json.Marshal(requestWebhook)
			if err != nil {
				log.Printf("Failed to marshall webhook call: %v", err)
			}
			_, err = http.Post(webHook.Url, "application/json", bytes.NewBuffer(requestBody))
			if err != nil {
				log.Printf("Not able to send webhook call: %v", err)
			}
		}
	}
}
