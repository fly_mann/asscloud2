package rest2

import (
	"encoding/json"
	"net/http"
	"sort"
	"strconv"
	"time"
)

//  count languages
func count_Languages(list []string) map[string]int {

	duplicate_frequency := make(map[string]int)
	for _, item := range list {
		// element inlist allready
		_, exist := duplicate_frequency[item]

		if exist {
			duplicate_frequency[item] += 1 // true count++
		} else {
			duplicate_frequency[item] = 1 // else count = 1
		}
	}
	return duplicate_frequency
}

func LanguagesHandler(w http.ResponseWriter, r *http.Request) {

	APIURL := "https://git.gvk.idi.ntnu.no/api/v4/projects"
	limit := getLimit(w, r) //  Gets limit

	languageKey := []string{} //

	var projects []Projects // Creating empty array
	projects = getProjects(w, r, APIURL)

	params := []string{strconv.Itoa(limit)}
	webHooksInvoke("languages", params, time.Now())

	//  all repo
	for _, i := range projects {

		APIURL1 := APIURL + "/" + strconv.Itoa(i.ID) + "/languages"

		if getAuth(w, r) != "" {
			APIURL1 = APIURL1 + "?" + getAuth(w, r)
		}

		singleProject, err := http.Get(APIURL1)

		var language = make(map[string]float32) //  empty map for languages
		err = json.NewDecoder(singleProject.Body).Decode(&language)

		if err != nil {
			http.Error(w, "Something went wrong: "+err.Error(), http.StatusBadRequest)
		}

		for k, _ := range language { //  add to list
			languageKey = append(languageKey, k)
		}
	}
	//  languages frequency map
	langMap := count_Languages(languageKey)
	var allLanguages = []LangName_Count{}

	for i, k := range langMap {
		var singleLanguage = LangName_Count{}
		singleLanguage.Name = i
		singleLanguage.Count = k

		allLanguages = append(allLanguages, singleLanguage)
	}
	//  Sort after languages
	sort.Slice(allLanguages, func(i, j int) bool { return allLanguages[i].Count > allLanguages[j].Count })

	//  Check if limit > max languages
	if limit > len(allLanguages) {
		limit = len(allLanguages)
	}
	//add language name to languagelist
	var outPut = Languages{}
	for i := 0; i < limit; i++ {
		outPut.LanguageList = append(outPut.LanguageList, allLanguages[i].Name)
	}
	outPut.Authentication = bool(getAuth(w, r) != "")
	json.NewEncoder(w).Encode(outPut)
}
