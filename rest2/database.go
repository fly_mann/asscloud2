package rest2

import (
	"context"
	"fmt"
	"log"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/option"
	/*
		"golang.org/x/net/context"

		firebase "firebase.google.com/go"
		"firebase.google.com/go/auth"

		"google.golang.org/api/option"
	*/)

// these are from Mariusz repo
// Init initiates the database
func (db *FirestoreDatabase) Init() error {
	db.Ctx = context.Background()
	var err error
	/*
		opt := option.WithCredentialsFile("path/to/serviceAccountKey.json")
		app, err := firebase.NewApp(context.Background(), nil, opt)
		if err != nil {
		  return nil, fmt.Errorf("error initializing app: %v", err)
		}
	*/
	//get json file for firbase on your computer
	sa := option.WithCredentialsFile("./rest2-fac4b-firebase-adminsdk-i1tvb-d96db3a678.json")

	db.Client, err = firestore.NewClient(db.Ctx, db.ProjectID, sa)
	if err != nil {
		fmt.Printf("Error in FirebaseDatabase.Init() function: %v\n", err)
		log.Fatal(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}

// Closes the DB connection
func (db *FirestoreDatabase) Close() {
	_ = db.Client.Close()
}

func (db *FirestoreDatabase) Save(s *Webhook) error {
	ref := db.Client.Collection(db.CollectionName).NewDoc()
	s.Id = ref.ID
	_, err := ref.Set(db.Ctx, s)
	if err != nil {
		fmt.Println("ERROR saving student to Firestore DB: ", err)
		log.Fatal(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}

// delete a webhook from the database based on the ID
func (db *FirestoreDatabase) Delete(s *Webhook) error {
	docRef := db.Client.Collection(db.CollectionName).Doc(s.Id)
	_, err := docRef.Delete(db.Ctx)
	if err != nil {
		fmt.Printf("ERROR deleting student (%v) from Firestore DB: %v\n", s, err)
		log.Fatal(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}
