module rest2

go 1.13.1

require (
	cloud.google.com/go/firestore v1.0.0
	google.golang.org/api v0.13.0
)
