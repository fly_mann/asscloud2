package rest2

import (
	"encoding/json"
	"net/http"
	"sort"
	"strconv"
	"time"
)

func CommitsHandler(w http.ResponseWriter, r *http.Request) {

	APIURL := "https://git.gvk.idi.ntnu.no/api/v4/projects"
	limit := getLimit(w, r) // limit = ?limit=x or 5 if null

	var projects []Projects // Creating empty array
	projects = getProjects(w, r, APIURL)

	// invokes the webhook for all those that are listening on commits
	params := []string{strconv.Itoa(limit)}       //
	webHooksInvoke("commits", params, time.Now()) //

	for k, v := range projects { //goes through all prosjekts

		APIURL1 := APIURL + "/" + strconv.Itoa(v.ID) + "/repository/commits"

		if getAuth(w, r) != "" {
			APIURL1 = APIURL1 + "?" + getAuth(w, r)
		}

		singleProject, err := http.Get(APIURL1)
		if err != nil {
			http.Error(w, "Something went wrong: "+err.Error(), http.StatusBadRequest)
		}

		projects[k].Commit, _ = strconv.Atoi(singleProject.Header.Get("X-Total"))
	}
	//sort after number of commit
	sort.Slice(projects, func(i, j int) bool { return projects[i].Commit > projects[j].Commit })

	// Check if limit > max repos
	if limit > len(projects) {
		limit = len(projects)
	}
	// array of length limit
	outPut := make([]RepoOutPut, limit)
	for k := range outPut {
		//output = adds projects[k] to outPut[k].Repositories[]
		outPut[k].Repositories = append(outPut[k].Repositories, projects[k])
		if getAuth(w, r) != "" {
			outPut[k].Authentication = true
		} else {
			outPut[k].Authentication = false
		}
	}

	json.NewEncoder(w).Encode(outPut)
}
