package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"rest2"
	"time"
)

func init() {
	rest2.TimeFromStart = time.Now()
}

func HandlerNil(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Default Handler: Invalid request received.")
	http.Error(w, "Invalid request", http.StatusBadRequest)
}

func main() {
	// firebase
	const projectID = "rest2-fac4b"
	const collection = "webhooks"

	//db = new database
	rest2.Db = rest2.FirestoreDatabase{ProjectID: projectID, CollectionName: collection}
	err := rest2.Db.Init()
	if err != nil {
		log.Fatal(err)
	}

	defer rest2.Db.Close()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	http.HandleFunc("/", HandlerNil)
	http.HandleFunc("/repocheck/v1/commits", rest2.CommitsHandler)
	http.HandleFunc("/repocheck/v1/languages", rest2.LanguagesHandler)
	///repocheck/v1/customwebhook
	http.HandleFunc("/repocheck/v1/webhooks", rest2.WebHooksHandler)
	http.HandleFunc("/repocheck/v1/webhooks/", rest2.WebHooksHandler)
	http.HandleFunc("/repocheck/v1/status", rest2.StatusHandler)
	fmt.Println("Listening on port " + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
